const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const {JWT_SECRET} = require('../config');
const {User} = require('../models/userModel');

const registration = async (req, res) => {
  const {email, password} = req.body;
  console.log(req.body);
  const role = req.body.role.toUpperCase();

  if (!email) {
    return res.status(400).json({message: `Please enter email`});
  }
  if (!password) {
    return res.status(400).json({message: `Please enter password`});
  }

  const user = new User({
    email,
    password: await bcrypt.hash(password, 10),
    role: role.toUpperCase(),
  });
  console.log(user);

  await user.save();

  res.status(200).json({message: 'Profile created successfully'});
};

const login = async (req, res) => {
  const {email, password} = req.body;

  const user = await User.findOne({email});

  if (!user) {
    return res.status(400).json({message: `${email} not found`});
  }
  if (!(await bcrypt.compare(password, user.password))) {
    return res.status(400).json({message: 'wrong password'});
  }
  const token = jwt.sign({
    username: user.email,
    _id: user._id,
  }, JWT_SECRET);

  res.json({jwt_token: token});
};

const forgotPassword = async (req, res) => {
  const {email} = req.body;
  const user = await User.findOne({email});

  if (!user) {
    return res.status(400).json({message: `${email} not found`});
  }

  res.status(200).json({message: 'New password sent to your email address'});
};


module.exports = {
  registration,
  login,
  forgotPassword,
};

