const {Load} = require('../models/loadsModel');
const {User} = require('../models/userModel');
const {Truck} = require('../models/truckModel');


const addLoad = async (req, res) => {
  const user = await User.findById(req.user._id);
  //   const user = await User.findById(req.locals._id);

  if (user.role != 'SHIPPER') {
    return res.status(400).json({message: 'User is not a shipper'});
  }

  const {name, payload, dimensions,
    pickup_address: pickup,
    delivery_address: delAddress} = req.body;

  const load = new Load({
    name,
    payload,
    dimensions,
    pickup_address: pickup,
    delivery_address: delAddress,
    created_by: req.user._id,
    logs: [{
      message: 'Load created',
    }],

  });

  await load.save();
  return res.status(200).json({message: 'Load created successfully'});
};


const getUserLoads = async (req, res) => {
  const user = req.user;
  let loads;
  if (user.role === 'DRIVER') {
    loads = await Load.find({assigned_to: user._id});
  } else {
    loads = await Load.find({created_by: user._id});
  }


  res.status(200).json({loads: loads});
};

const getActiveLoads = async (req, res) => {
  const userId = req.user._id;
  const load = await Load.findOne(
      {assigned_to: userId, status: 'ASSIGNED'},
      {__v: 0},
  );
  return res.json({load});
};


const changeDriverActiveLoadInfo = async (req, res) => {
  const activeLoad = await Load.findOne({
    assigned_to: req.user._id,
    status: 'ASSIGNED',
  });


  if (activeLoad.state == 'En route to Pick Up') {
    await Load.findByIdAndUpdate(activeLoad._id, {
      $set: {state: 'Arrived to Pick Up'},
      logs: [
        {
          message: 'Arrived to Pick Up',
        },
      ],
    });
    return res.json({
      message: 'Load state changed to \'Arrived to Pick Up\'',
    });
  }
  if (activeLoad.state == 'Arrived to Pick Up') {
    await Load.findByIdAndUpdate(activeLoad._id, {
      $set: {state: 'En route to delivery'},
      logs: [
        {
          message: 'En route to delivery',
        },
      ],
    });
    return res.json({
      message: 'Load state changed to \'En route to delivery\'',
    });
  }
  if (activeLoad.state == 'En route to delivery') {
    await Load.findByIdAndUpdate(activeLoad._id, {
      $set: {
        state: 'Arrived to delivery',
        status: 'SHIPPED',
        logs: [
          {
            message: 'Your load is shipped',
          },
        ],
      },
    });
    await Truck.findOneAndUpdate(
        {assigned_to: req.user._id},
        {$set: {status: 'IS', assigned_to: null}},
    );
    return res.json({
      message: 'Load state changed to \'Arrived to delivery\'',
    });
  }
};


const getUserLoad = async (req, res) => {
  const {id} = req.params;
  const load = await Load.findById(id, {__v: 0});
  res.status(200).json({load: load});
};


const deleteUserLoad = async (req, res) => {
  const {id} = req.params;

  const load = await Load.findById(id);
  if (load.status === 'NEW') {
    await Load.findByIdAndRemove({_id: id});
  }
  res.status(200).json({message: 'Load deleted successfully'});
};

const editUserLoad = async (req, res) => {
  console.log(req.body);
  const {id} = req.params;

  const body = req.body;
  const infoToUpdate = {};

  for (const property in body) {
    if (body[property]) {
      infoToUpdate[property] = body[property];
    }
  }

  const load = await Load.findById(id);

  if (load.status === 'NEW') {
    await Load.findByIdAndUpdate({_id: id}, infoToUpdate );
    res.status(200).json({message: 'Load details changed successfully'});
  }
};

const postLoad = async (req, res) => {
  const {id} = req.params;
  const load = await Load.findById(id);


  load.logs.push({message: 'status updated'});
  await Load.findByIdAndUpdate({_id: id}, {status: 'POSTED', logs: load.logs} );

  const truck = await Truck
      .where('assigned_to').ne(null)
      .where('status').equals('IS')
      .where('payload').gt(load.payload)
      .where('dimensions.width').gt(load.dimensions.width)
      .where('dimensions.length').gt(load.dimensions.length)
      .where('dimensions.height').gt(load.dimensions.height)
      .findOne();

  if (!truck) {
    load.logs.push({message: 'status returned to new'});
    await Load.findByIdAndUpdate({_id: id},
        {status: 'NEW', logs: load.logs} );
  }
  truck.status = 'OL';
  await truck.save();

  const {assigned_to: driverId} = truck;

  load.assigned_to = driverId;
  load.status = 'ASSIGNED';
  load.state = 'En route to Pick Up';
  load.logs.push({message: 'Load has been assigned'});
  await load.save();

  res.status(200).json({'message': 'Load posted successfully',
    'driver_found': true});
};

const getShippingInfo = async (req, res) => {
  const user = req.user;
  const _id = req.params.id;
  const load = await Load.findOne({_id, created_by: user._id});
  const truck = await Truck.findOne({assigned_to: load.assigned_to},
      {__v: 0, dimensions: 0, payload: 0});
  res.status(200).json({load: load, truck: truck});
};


module.exports = {
  addLoad,
  getUserLoads,
  getUserLoad,
  deleteUserLoad,
  editUserLoad,
  postLoad,
  getShippingInfo,
  getActiveLoads,
  changeDriverActiveLoadInfo,
};

