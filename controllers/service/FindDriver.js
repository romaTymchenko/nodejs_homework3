const {Truck} = require('../../models/truckModel');
const cars = require('../service/trackParamets');
// const {Truck} = require('../../models/truckModel');

const findFreeTruck = async () => {
  try {
    const truckList = await Truck.find({status: 'IS'});

    if (truckList.length === 0) return null;

    return truckList;

    //     const load = await Load.findById(id);
    //     for (const truck of truckList) {
    //       const loadFits = this.isLoadFit(load, truck);
    //       if (loadFits) {
    //         return truck;
    //       }
    //     }
    //     return null;
  } catch (err) {
    throw new Error(err.message);
  }
};

module.exports = findFreeTruck;
