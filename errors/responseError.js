/** Class representing a statusCode. */
class ResponseError extends Error {
  /**
        * @param {string} message - The message value.
         */
  constructor(message = 'No response') {
    super(message);
    this.statusCode = 400;
  }
}


module.exports = ResponseError;
