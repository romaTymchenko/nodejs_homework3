const express = require('express');
const router = new express.Router();
const {asyncWrapper} = require('./helpers');
const {authMiddleware} = require('./middleware/authMiddleware');


const {
  getUserInfo,
  deleteUser,
  changeUserPassword,
} = require('../controllers/usersController');


router.get('/me', authMiddleware, asyncWrapper(getUserInfo));
router.delete('/me', authMiddleware, asyncWrapper(deleteUser));
router.patch('/me/password', authMiddleware, asyncWrapper(changeUserPassword));

module.exports = router;
