const isLoadExists = (Load) => async (req, res, next) => {
  const {id} = req.params;

  try {
    if (! await Load.exists({_id: id})) {
      return res.status(400).json({message: 'No load with this id'});
    }
    next();
  } catch (e) {
    return res.status(400).json({message: 'No load with this id'});
  }
};

module.exports = {
  isLoadExists,
};
