const express = require('express');
const router = new express.Router();

const {asyncWrapper} = require('./helpers');
const {validateRegistration} = require('./middleware/validationMiddleware');
const {login,
  registration,
  forgotPassword} = require('../controllers/authControllers');

router.post('/register',
    asyncWrapper(validateRegistration),
    asyncWrapper(registration));
router.post('/login', asyncWrapper(login));
router.post('/forgot_password', asyncWrapper(forgotPassword));

module.exports = router;
