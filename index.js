const mongoose = require('mongoose');
const express = require('express');
const morgan = require('morgan');
const app = express();
const cors = require('cors');
require('dotenv').config();
const port = process.env.PORT || 8090;
const ResponseError = require('./errors/responseError');


const authRouter = require('./routers/authRouter');
const usersRouter = require('./routers/usersRouter');
const trucksRouter = require('./routers/trucksRouter');
const loadsRouter = require('./routers/loadsRouter');
app.use(cors());

app.use(express.json());
app.use(express.static('build'));
app.use(morgan('tiny'));

app.use('/api/auth', authRouter);
app.use('/api/users', usersRouter);
app.use('/api/', trucksRouter);
app.use('/api/', loadsRouter);


/** Class representing a statusCode. */
class UnauthorizedError extends Error {
  /**
    * @param {string} message - The message value.
     */
  constructor(message = 'Unauthorized user') {
    super(message);
    statusCode = 400;
  }
}

app.use((err, req, res, next) => {
  if (err instanceof UnauthorizedError) {
    res.status(err.statusCode).json({message: err.message});
  }
  if (err instanceof ResponseError) {
    res.status(err.statusCode).json({message: err.message});
  }
  res.status(500).json({message: err.message});
});

const start = async () => {
  await mongoose.connect('mongodb+srv://myuser:mypassword@cluster0.ywogq.mongodb.net/hwdata?retryWrites=true&w=majority', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true,
  });

  app.listen(port, () => {
    console.log(`Server is running at port ${port}`);
  });
};

start();


